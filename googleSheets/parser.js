var getGoogleSheet = require('./sheets');
var Name = require('../models/Name');
var Origin = require('../models/Origin');

var i = {
    title: 0,
    gender: 1,
    origin: 2,
    meaning: 3,
    description: 4,
    celebrities: 5,
    category: 6,
    nameDay: 7
}
var namesParcer = async function(sheetUrl, range) {
    try{

        let data = await getGoogleSheet(sheetUrl, range); //таблица
        if(data.length == 0) {
            return {
                message: 'sheet is empty'
            }
        }
        data.splice(0, 1); // удаляем первую строку, в которой хранятся названия столбцов

        for (const str of data) {
            if(str[i.title] && str[i.origin]) {
                console.log(str[i.title])
                let name = await Name.findOne({title: str[i.title].toLowerCase()});

                if(!name) {
                    let origin = await Origin.findOne({title: str[i.origin].toLowerCase()});

                        let nameObj = {
                            title: str[i.title].toLowerCase(),
                            gender: str[i.gender] === 'Мужской'? true : false,
                            origin: '',
                            meaning: str[i.meaning],
                            description: str[i.description],
                            category: str[i.category],
                            nameDay: str[i.nameDay]
                        }
                        if(!origin) {
                            let newOrigin = new Origin({title: str[i.origin].toLowerCase()});
                            newOrigin = await newOrigin.save();
                            nameObj.origin = newOrigin._id;
                            let newName = new Name(nameObj)
                            await newName.save();
                        } else {
                            nameObj.origin = origin._id;
                            let newName = new Name(nameObj);
                            await newName.save();
                        }
                    

                }
            
            }
        }
        return {
            message: 'ok'
        }

    } catch (e) {
        throw new Error('error in namesParcer :::' + e.message)
    }
}

module.exports = namesParcer;