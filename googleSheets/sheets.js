const { google } = require('googleapis');
const keys = require('./keys.json'); 


const client = new google.auth.JWT(
    keys.client_email, 
    null, 
    keys.private_key,
    ['https://www.googleapis.com/auth/spreadsheets.readonly']
)

async function getGoogleSheet(sheetUrl, range) {
    const options = {
        spreadsheetId: parseUrl(sheetUrl),
        range: range
    }

    console.log('OPTIONS::: ', options);

    let run = new Promise((res, rej) => {
        client.authorize(async function(err, tokens) {
            if(err) {
                console.log(err);
                throw new Error('err')
                return;
            }
            console.log('Connected')
            let result = await gsrun(client, options);
            res(result)
        })
    })
    let data = await run;
    return data;
}

async function gsrun(client, options) {

    try{
        const gsapi = google.sheets( { version: 'v4', auth: client } );

        let data = await gsapi.spreadsheets.values.get(options);
        return(data.data.values)
    } catch (e) {
        throw e;
    }

}

function parseUrl (url) {
    let regex = new RegExp("/spreadsheets/d/([a-zA-Z0-9-_]+)");
    let result = url.match(regex)[1];
    return result;
}

module.exports = getGoogleSheet;