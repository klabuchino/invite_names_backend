let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let schema = new Schema({
    title: {
        type: String
    },
    gender: {
        type: Boolean,
        default: true
    },
    origin: {
        type: Schema.Types.ObjectId,
        ref: 'Origin'
    },
    meaning: {
        type: String
    },
    description: {
        type: String
    },
    category: {
        type: String
    },
    nameDay: {
        type: String
    }
})

schema.statics.get_one = async function(name_id) {
    let Self = this;
    try{
        let name = await Self.findOne({_id: name_id}).populate('origin');
        if (name)
            return name;
        throw new Error('name is not exist')
    } catch(e) {
        throw e
    }
}

schema.statics.get = async function(filters) {
    let Self = this;
    console.log(filters);
    try{ 
        let filtersObj = create_filtersObj(filters)
        let names = await Self.find(filtersObj);
        return names
    } catch(e) {
        throw e
    }
}

schema.statics.findNames = async function(name) {
    let Self = this;
    try {
        if(name.length == 0) return [];
        
        let regexName = new RegExp('^' + name, 'i')
        let names = await this.find( { title: { $regex : regexName } } )
        return names
    } catch (e) {
        throw e;
    }
}

var create_filtersObj = function (filters) {
    if(filters.origins.length > 0 && filters.genders.length > 0) {
        return {
            $and: [
                {
                    origin: {$in: filters.origins}
                },
                {
                    gender: {$in: filters.genders}
                }
            ]   
        }
    }
    if(filters.origins.length > 0 && filters.genders.length == 0) {
        return {
            origin: {$in: filters.origins}
        }
    }
    if(filters.origins.length == 0 && filters.genders.length > 0) {
        return {
            gender: {$in: filters.genders}
        }
    }
    return {}
}



module.exports = Name = mongoose.model('Name', schema);