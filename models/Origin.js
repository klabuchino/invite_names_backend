let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let schema = new Schema({
    title: {
        type: String
    }
})

schema.statics.get = async function() {
    const Self = this;
    try {
        let origins = await this.find({});
        return origins;
    } catch (e) {
        throw e
    }
}
module.exports = Origin = mongoose.model('Origin', schema);