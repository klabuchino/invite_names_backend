let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let schema = new Schema({
    favorites_names: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Name'
        }
    ]
})

schema.statics.create = async function() {
    let Self = this;
    try {
        let new_user = new Self({})
        new_user = await new_user.save();
        return new_user
    } catch {
        throw new Error('failed create user');
    }
}

schema.statics.add_favorites_names = async function(user_id, favorites_name_id) {
    let Self = this;
    try{
        let user = await Self.findById(user_id);
        if(!user)
            throw new Error('user not exist')
        isNameIncludes = user.favorites_names.includes(favorites_name_id);

        if(!isNameIncludes) user.favorites_names.push(favorites_name_id);

        user = await user.save();
    } catch(e) {
        throw new Error('failed add favorites name ' + e.message);
    }
}
schema.statics.remove_favorites_names = async function(user_id, favorites_name_id) {
    let Self = this;
    try{
        let user = await Self.findById(user_id);
        let new_favorites_names = user.favorites_names.filter(name => name.toString() != favorites_name_id);
        await Self.update({_id: user_id}, {favorites_names: new_favorites_names});
    } catch(e) {
        throw e;
    }
}

schema.statics.get = async function(user_id) {
    let Self = this;
    try {
        let user = await Self.findById(user_id).populate('favorites_names');
        if(user) return user
        throw new Error('user not exist')
    } catch(e) {
        throw e;
    }
}

module.exports = User = mongoose.model('User', schema);