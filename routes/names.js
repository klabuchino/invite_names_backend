var express = require('express');
var router = express.Router();
var Name = require('../models/Name');
var Origin = require('../models/Origin');
var parser = require('../googleSheets/parser');


router.get('/get_one', async function(req, res, next) {
    try {
        let name = await Name.get_one(req.query.name_id);
        res.json(name);
    } catch(e) {
        console.log(e.message);
        res.json({error: {status:'500', message: e.message}})
    }
})

router.post('/get', async function(req, res, next) {
    try {
        let names = await Name.get(req.body);
        res.json(names);
    } catch(e) {
        console.log(e.message);
        res.json({error: {status: '500', message: e.message}})
    }
})

router.get('/get_origins', async function(req, res, next) {
    try {
        let origins = await Origin.get();
        res.json(origins)
    } catch (e) {
        res.json({error: {status: '500', message: e.message}})
    }
})

router.post('/find', async function(req, res, next) {
    try {
        let names = await Name.findNames(req.body.name);
        res.json(names);
    } catch (e) {
        res.json({error: {status: '500', message: e.message}})
    }
})

router.post('/sendSheet', async function(req, res, next) {
    try {
        let message = await parser(req.body.url, req.body.range);
        res.json(message);
    } catch (e) {
        res.json({error: {status: '500', message: e.message}})
    }
})

module.exports = router;