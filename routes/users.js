var express = require('express');
var router = express.Router();
var User = require('../models/User');

router.post('/create', async function(req, res, next) {
  try {
    let user = await User.create();
    res.json(user);
  } catch(e) {
    console.log(e.message);
    res.json({error: {status: '500', message: e.message}});
  }
});

router.get('/get', async function(req,res, next) {
  try {
    let user = await User.get(req.query.user_id);
    res.json(user)
  } catch(e) {
    console.log(e.message)
    res.json({error: {status: '400', message: e.message}});
  }
})

router.post('/add_favorites_names', async function(req, res, next) {
  try{
    await User.add_favorites_names(req.body.user_id, req.body.favorites_name_id);
    let user = await User.get(req.body.user_id);
    res.json(user);
  } catch(e) {
    console.log(e.message);
    res.json({error: { status: '500', message: e.message}});
  }
})

router.get('/clear_favorites', async function(req, res, next) {
  let user = await User.update({_id: req.query.user_id}, {favorites_names: []})
  res.json(user)
})

router.post('/remove_favorites', async function(req, res, next) {
  try {
    await User.remove_favorites_names(req.body.user_id, req.body.favorites_name_id);
    let user = await User.get(req.body.user_id);
    res.json(user);
  } catch (e) {
      console.log('remove favorites::: ', e.message);
      res.json({error: { status: '500', message: e.message}});
  }
})

module.exports = router;
