let Origin = require('./models/Origin')
module.exports = {
    origins: [
        {
            title: 'Казахские'
        },
        {
            title: 'Русские'
        },
        {
            title: 'Английские'
        },
        {
            title: 'Славянские'
        },
        {
            title: 'Китайские'
        },
        {
            title: 'Испанские'
        },
        {
            title: 'Украинские'
        },
        {
            title: 'древнегреческие'
        }
    ],
    names: [
        {
            title: 'Александр',
            gender: true,
            origin: 'древнегреческие',
            meaning: 'Защитник, оберегающий муж',
            declension: ['Александр', 'Александра', 'Александру', 'Александра', 'Александром','Александре'],
            diminutive: 'Саня, Санька, Саша, Сашка, Сашуня, Шура, Шурка, Шурик, Алекся, Алексюша',
            patronymic: 'Александрович / Aлександровна'
        },
        {
            title: 'Оксана',
            gender: false,
            origin: 'Украинские',
            meaning: 'Иностранка, странница',
            declension: ['Оксана', 'Оксаны', 'Оксане', 'Оксана', 'Оксаной','Оксане'],
            diminutive: 'Оксанка, Ксана, Сана, Ксюня, Сюня, Ксюра, Ксюта, Ксюша',
            patronymic: ''
        },
        {
            title: 'Алтын',
            gender: false,
            origin: 'Казахские',
            meaning: 'Золото, золотая',
            declension: ['Алтын', 'Алтын', 'Алтын', 'Алтын', 'Алтын','Алтын'],
            diminutive: '',
            patronymic: ''
        },
        {
            title: 'Мстислав',
            gender: true,
            origin: 'Славянские',
            meaning: 'Славный защитник',
            declension: ['Мстислав', 'Мстислава', 'Мстиславу', 'Мстислав', 'Мстиславом','Мстиславе'],
            diminutive: 'Мстята, Мстиша, Мстишенька, Стива, Мстиславчик, Слава, Славик',
            patronymic: 'Мстиславович / Мстиславовна'
        }
    ],
    create_origins: async function(origins) {
        for await(let origin of origins) {
            console.log(origin)
            let new_origin = new Origin(origin);
            new_origin.save();
        }
    },
    create_names: async function(names) {
        for await(let name of names) {
            console.log(name);
            create_name(name);
        }
    }
}

const create_name = async (name) => {
    let origin = await Origin.findOne({title: name.origin});
    nameObj = {
        ...name,
        origin: origin._id.toString()
    }
    let new_name = new Name(nameObj);
    new_name = await new_name.save();
}