var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var mongoose = require('mongoose');
var session = require('express-session');
var config = require('./config');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var namesRouter = require('./routes/names');

mongoose.set('useUnifiedTopology', true);
mongoose.connect(config.mongoUrl, { useNewUrlParser: true });

var MongoStore = require('connect-mongo')(session);

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mongoose.set('useCreateIndex', true);

config.session.store = new MongoStore({mongooseConnection: mongoose.connection});
app.use(session(config.session));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/names', namesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});
  
  // error handler
app.use(function(err, req, res, next) {
// set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
});
module.exports = app;
